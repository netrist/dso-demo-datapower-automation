# Datapower Automation

This project provides an automation tool for launching SOAP APIs to the Datapower. It uses the Datapower's "SOMA" interface which allows set-up of Datapower configurations through an API.

Run scripts from the scripts folder.
Files are assumed to be in the files folder.

Variables:

~~~
 1 = hostname of the xsg 
 2 = admin username
 3 = admin password
 4 = domain to create and import to
 5 = zip file to import, expected to be an export of a domain
~~~

Example:

`./createImportConfiguration.sh localhost admin admin genericDomain genericDomainExport.zip`

## Try it yourself!

In the example folder, you will find a domain export for a NAICS web service. For testing, you can run Datapower in docker and then use this tool to import the NAICS web service into Datapower.

### Running Datapower in Docker

Run a Datapower container with docker (you must have docker installed on your desktop or server):

```
docker run -it -p 9090:9090 -p 9022:22 -p 5550:5550 -p 5554:5554 -p 8080:8080 \
	-e DATAPOWER_ACCEPT_LICENSE=true \
	-e DATAPOWER_INTERACTIVE=true \
	-e DATAPOWER_WORKER_THREADS=4 \
	ibmcom/datapower
```

Enable the web GUI and XML Managment by entering these commands, the default login is admin/admin:

Note: Sometimes an addtional enter key push is needed to return to the prompt
```
co
web-mgmt 0 9090 9000
xml-mgmt
admin-state enabled
exit
write mem
```

### Test the Tool

Import the sample NAICS domain and SOAP API using the following command:

```
./createImportConfiguration.sh \
	localhost \ 
	admin \
	admin \
	genericNAICS \
	genericNAICSXSGExport.zip
```

Log-into the web GUI at [https://localhost:9090](https://localhost:9090) to confirm the domain was imported! (NOTE: Since Datapower uses a self-signed, untrusted certificate, you will need to adjust your browser security settings accordlingly. Firefox is recommended.)
