#!/bin/bash

#Set global variables for folder locations
scriptsFolder=$PWD/../scripts
requestFolder=$PWD/../requestXML
tempFiles=$PWD/../tempFiles
files=$PWD/../files

#Source the functions script
. ${scriptsFolder}/cliXSGFunctions.sh

#Variables
# 1 = hostname of the xsg
# 2 = username
# 3 = password
# 4 = domain
# 5 = zip file to import


set_connection_information $1 $2 $3

create_application_domain $4

import_configuration $4 $5
