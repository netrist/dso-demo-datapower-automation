#!/bin/bash

scriptsFolder=$PWD/../scripts
requestFolder=$PWD/../requestXML
tempFiles=$PWD/../tempFiles

. ${scriptsFolder}/cliXSGFunctions.sh

#Variables
# 1 = hostname of the xsg
# 2 = username
# 3 = password
# 4 = domain
# 5 = zip file to import

set_connection_information $1 $2 $3

delete_application_domain $4
