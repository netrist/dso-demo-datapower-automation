#!/bin/bash

  LOGFILE=/tmp/assist_deploy.log

  log() {
          while read IN
          do
                  echo "$IN"
                  echo "$IN" >> $LOGFILE
          done
  }

  get_connection_information(){

	echo "XSG to connect to:" | log
	read xsg
	echo "XSG Admin Username:" | log
 	read username
    	echo "XSG Admin Password:" | log
    	read -s password

	set_connection_information $xsg $username $password
  }

  set_connection_information(){

	xsg=$1
 	username=$2
    	password=$3

	target_url=https://$xsg:5550/service/mgmt/3.0
  }


  check_response(){
	local responseFile=$1
	local requestFile=$2

	responseStatus=$(xmlstarlet sel -T -N dp="http://www.datapower.com/schemas/management" -t -v "//dp:result" ${tempFiles}/$responseFile | tr -d " \n")

	#Check the Status of the response
  	if [[ ${responseStatus} != "OK" ]]; then
    		echo "Unexpected Response Status ${responseStatus} for:  $responseFile"
		exit 1
  	else
    		#Reponse returned OK
    		#Delete the Response File
    		rm ${tempFiles}/$responseFile

    		#Delete the request XML
    		rm ${tempFiles}/$requestFile
  	fi;
  }

  check_import_response(){
	local responseFile=$1
	local requestFile=$2

	responseStatus=$(xmlstarlet sel -T -N dp="http://www.datapower.com/schemas/management" -t -v "//dp:import" ${tempFiles}/$responseFile | tr -d " \n")

	#Check the Status of the response
  	if [[ -z ${responseStatus} ]]; then
    		echo "Unexpected Response Status ${responseStatus} for:  $responseFile"
		exit 1
  	else
    		#Response returned OK
    		#Delete the Response File
    		rm ${tempFiles}/$responseFile

    		#Delete the request XML
    		rm ${tempFiles}/$requestFile
  	fi;
  }


  create_application_domain(){
	local domainString=$1

	echo "Creating application domain $domainString"
	sed  's!DOMAIN!'"$domainString"'!' ${requestFolder}/createApplicationDomainTemplate.xml > ${tempFiles}/createApplicationDomainRequest.xml

  	#Create the application domain in Default on the XSG with an Admin Account
  	curl -ksS -u $username:$password -d @${tempFiles}/createApplicationDomainRequest.xml ${target_url} > ${tempFiles}/createApplicationDomainResponse.xml

	#Check the response to make sure it was ok
	check_response createApplicationDomainResponse.xml createApplicationDomainRequest.xml

	#Save the domain
	save_domain_configuration default

  }

  delete_application_domain(){
	local domainString=$1

  	echo "Deleting application domain $domainString"
  	sed  's!DOMAIN!'"$domainString"'!' ${requestFolder}/deleteApplicationDomainTemplate.xml > ${tempFiles}/deleteApplicationDomainRequest.xml

  	#Delete the application domain in Default on the XSG with an Admin Account
  	curl -ksS -u $username:$password -d @${tempFiles}/deleteApplicationDomainRequest.xml ${target_url} > ${tempFiles}/deleteApplicationDomainResponse.xml

	#Check the response to make sure it was ok
	check_response deleteApplicationDomainResponse.xml deleteApplicationDomainRequest.xml

	#Save the domain
	save_domain_configuration default

  }

  
  import_configuration(){
	local domainString=$1
	local importFileString=${files}/$2


	echo "Importing configuration for $domainString"

	#Encode the routing utils file as an encoded string
	base64  $importFileString > ${tempFiles}/encodedFileString.txt

	encodedFileString=$(cat ${tempFiles}/encodedFileString.txt)

	#Create a Request XML file from the template with the encoded String
	sed  's!DOMAIN!'"$domainString"'!' ${requestFolder}/importDomainTemplate.xml > ${tempFiles}/importDomainRequest.xml

	#Use ed since the encode string is too big for sed
	printf '%s\n' '/BASE64ENCODEDFILE/r '${tempFiles}'/encodedFileString.txt' 1 '/BASE64ENCODEDFILE/d' w | ed ${tempFiles}/importDomainRequest.xml
	#sed  -f 's!BASE64ENCODEDFILE!'"$encodedFileString"'!' ${tempFiles}/importDomainRequest.xml > ${tempFiles}/importDomainRequest.xml

	#Import a configuration to a domain the XSG with an Admin Account
	curl -ksS -u $username:$password -d @${tempFiles}/importDomainRequest.xml ${target_url} | sed 's/<?xml version="1.0" encoding="UTF-8"?>//' | sed 's@.*<dp:file>\(.*\)</dp:file>.*@\1@g' > ${tempFiles}/importDomainResponse.xml

	#Check the response to make sure it was ok
	check_import_response importDomainResponse.xml importDomainRequest.xml

	#Save the domain
	save_domain_configuration $domainString
	
	#Delete the encode file
    	rm ${tempFiles}/encodedFileString.txt
  }
  
  save_domain_configuration(){
	local domainString=$1
	
	echo "Saving the configuration in application domain $domainString"
	sed  's!DOMAIN!'"$domainString"'!' ${requestFolder}/saveConfigTemplate.xml > ${tempFiles}/saveConfigRequest.xml

	#Save the Configuration
	curl -ksS -u $username:$password -d  @${tempFiles}/saveConfigRequest.xml ${target_url}  > ${tempFiles}/saveConfigResponse.xml

	#Check the response to make sure it was ok
	check_response saveConfigResponse.xml saveConfigRequest.xml

  }
